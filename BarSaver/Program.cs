﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NDesk.Options;

namespace BarSaver
{
    class Program
    {
        static void Main(string[] args)
        {
            string product = null;
            string file = null;
            string ip = null;

            OptionSet options = new OptionSet().Add("p=|product=", p => product = p).Add("f=|file=", f => file = f).Add("i=|ip=", i => ip = i);
            options.Parse(args);
            if (product == null || file == null || ip == null)
            {
                Console.Error.WriteLine("Error in Options");
            }

            Utilities.UploadFiveMinuteBarsToDB(product, file, ip);
        }
    }
}
