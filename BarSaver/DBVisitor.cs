﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace BarSaver
{
    /// <summary>
    /// This class provides the basic functions for accessing database
    /// </summary>
    public class DBVisitor
    {
        public string connectString = "server=192.168.18.181,30000;uid=vegasoul;pwd=vincent1;database=ConfigManager";
        public SqlConnection myCon;


        public DBVisitor(string connectString)
        {
            this.connectString = connectString;
            myCon = null;

        }

        public bool ConnectDB()
        {
            bool result = true;
            try
            {
                if (myCon == null || myCon.State != System.Data.ConnectionState.Open)
                {
                    myCon = new SqlConnection(this.connectString);
                    //myCon.ConnectionTimeout = 120;
                    myCon.Open();
                }

            }
            catch (Exception e)
            {


                result = false;
                if (myCon != null && myCon.State == System.Data.ConnectionState.Open)
                {
                    myCon.Close();
                }
                Console.WriteLine(e.ToString());
                Console.ReadLine();
            }
            finally
            {

            }
            return result;

        }

        public void CloseDB()
        {

            if (myCon != null && myCon.State == System.Data.ConnectionState.Open)
            {
                myCon.Close();
            }
            myCon = null;

        }


        /// <summary>
        /// This method executes a non query sql, and return whether the execution success or not.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public bool ExecuteNonQuery(string sql)
        {
            bool result = true;
            SqlCommand myCommand = this.myCon.CreateCommand();
            SqlTransaction myTrans;
            //begin transaction
            myTrans = this.myCon.BeginTransaction();
            myCommand.Connection = myCon;
            myCommand.Transaction = myTrans;
            try
            {
                myCommand.CommandText = sql;
                myCommand.ExecuteNonQuery();
                myTrans.Commit();
            }
            catch (Exception e)
            {
                try
                {
                    myTrans.Rollback();
                }
                catch (SqlException ex)
                {

                    Console.WriteLine(ex.ToString());
                }
                Console.WriteLine(e.ToString());
                Console.ReadLine();

                result = false;
            }

            return result;
        }

        /// <summary>
        /// This method executes a sql to query data from database.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable queryData(string sql)
        {
            DataSet myDataSet = new DataSet();
            DataTable myTable = null;
            SqlCommand myCommand = this.myCon.CreateCommand();
            //SqlTransaction myTrans;
            //begin transaction
            //myTrans = this.myCon.BeginTransaction();
            myCommand.Connection = myCon;
            myCommand.CommandTimeout = 3000;
            //myCommand.Transaction = myTrans;
            try
            {
                SqlDataAdapter custDa = new SqlDataAdapter();
                custDa.SelectCommand = myCommand;
                myCommand.CommandText = sql;

                custDa.Fill(myDataSet, "DATA");
                myTable = myDataSet.Tables["DATA"];
                //myTrans.Commit();

            }
            catch (Exception e)
            {
                try
                {
                    //myTrans.Rollback();
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.ToString());

                }
                myTable = null;

                Console.WriteLine(e.ToString());
                Console.ReadLine();
            }
            return myTable;
        }

    }
}
