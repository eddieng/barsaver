﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace BarSaver
{
    class Utilities
    {

        // Helpers

        public static void UploadFiveMinuteBarsToDB(string productName, string filename, string ip)
        {
            Dictionary<Tuple<DateTime, DateTime>, Tuple<double, double, double, double, double>> bars = new Dictionary<Tuple<DateTime, DateTime>, Tuple<double, double, double, double, double>>();
            DateTime minTradeDate = DateTime.MaxValue;

            // Read from file
            List<string> lines = File.ReadLines(filename).ToList();
            foreach (string line in lines)
            {
                string[] tokens = line.Split(new char[] { ',' });
                try
                {
                    DateTime tradeDate = DateTime.ParseExact(tokens[0], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (tradeDate < minTradeDate)
                    {
                        minTradeDate = tradeDate;
                    }
                    DateTime endTime = DateTime.ParseExact(tokens[1], "HH:mm", CultureInfo.InvariantCulture);
                    double openPx = Double.Parse(tokens[2]);
                    double highPx = Double.Parse(tokens[3]);
                    double lowPx = Double.Parse(tokens[4]);
                    double closePx = Double.Parse(tokens[5]);
                    double volume = Double.Parse(tokens[6]);
                    Tuple<DateTime, DateTime> barKey = new Tuple<DateTime, DateTime>(tradeDate, endTime);
                    if (!bars.ContainsKey(barKey))
                    {
                        bars.Add(barKey, new Tuple<double, double, double, double, double>(openPx, highPx, lowPx, closePx, volume));
                    }
                }
                catch (Exception ex)
                {
                    continue;
                }
            }

            // To SQL
            string deleteSql = "DELETE FROM [ConfigManager].[dbo].[FiveMinuteBar] WHERE [ProductName] = '" + productName + "' AND [TradeDate] >= '" + minTradeDate.ToString("yyyy-MM-dd") + "'";
            List<string> sqls = new List<string>();
            string sql = "INSERT INTO [ConfigManager].[dbo].[FiveMinuteBar] ([ProductName], [TradeDate], [EndTime], [OpenPx], [HighPx], [LowPx], [ClosePx], [Volume], [ImportDate]) VALUES ";
            string tmpSql = "";
            int count = 0;
            foreach (var bar in bars.Keys)
            {
                tmpSql += (tmpSql == "" ? "" : ", ") + "('" + productName + "', '" + bar.Item1.ToString("yyyy-MM-dd") + "', '" + bar.Item2.ToString("HH:mm") + "', " + bars[bar].Item1 + ", " + bars[bar].Item2 + ", " + bars[bar].Item3 + ", " + bars[bar].Item4 + ", " + bars[bar].Item5 + ", '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.000") + "')";
                ++count;
                if (count >= 999)
                {
                    sqls.Add(sql + tmpSql);
                    count = 0;
                    tmpSql = "";
                }
            }
            if (tmpSql != "")
            {
                sqls.Add(sql + tmpSql);
            }

            string connectionStr = "server=" + ip + ";uid=vegasoul;pwd=vincent1;database=ConfigManager;Connection Timeout=120";
            DBVisitor DBvisitor = new DBVisitor(connectionStr);
            DBvisitor.ConnectDB();
            DBvisitor.ExecuteNonQuery(deleteSql);
            foreach (string s in sqls)
            {
                DBvisitor.ExecuteNonQuery(s);
            }
            DBvisitor.CloseDB();
        }

    }
}
